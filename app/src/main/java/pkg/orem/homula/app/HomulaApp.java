package pkg.orem.homula.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import pkg.orem.homula.R;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

@ReportsCrashes(mailTo = "sukhwinderkaur.orem@gmail.com")
public final class HomulaApp extends android.app.Application {

    private static Context context;



    public static String Font_Text = "fonts/calibri.ttf";
    private static HomulaApp mInstance;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
        mInstance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(Font_Text)
                .setFontAttrId(R.attr.fontPath)
                .build());

        context = null;
        context = getApplicationContext();
    }

    public static Context getGlobalContext() {
        return context;
    }

    public static Resources getAppResources() {
        return context.getResources();
    }


    public static synchronized HomulaApp getInstance() {
        return mInstance;
    }

    public void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Regular.ttf");
        SpannableString fontTitle = new SpannableString(mi.getTitle());
        fontTitle.setSpan(new CalligraphyTypefaceSpan(font), 0, fontTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(fontTitle);
    }
    public void hideSoftKeyBoard(View view){
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public boolean isValidEmail(EditText editText) {
        if (!Validation.hasText(editText, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(editText, true, getString(R.string.error_email_invalid));
    }

    public boolean isValidatePassword(EditText editText) {
        if (!Validation.hasText(editText, getString(R.string.err_empty_password))) {
            return false;
        }
        return Validation.isValidPassword(editText, getString(R.string.error_pass_invalid));
    }

    public boolean isValidConfirmPassword(EditText nPwdEdt, EditText cPwdEdt) {
        if (!Validation.hasText(cPwdEdt, getString(R.string.err_confirm_pwd))) {
            return false;
        }
        if (!Validation.isValidPassword(cPwdEdt, getString(R.string.error_pass_invalid))) {
            return false;
        }
        return Validation.isConfirmPassword(nPwdEdt, cPwdEdt, getString(R.string.password_not_match));
    }

    public boolean isValidPhone(EditText editText) {
        return Validation.hasText(editText, getString(R.string.err_empty_phone_number));
    }

    public boolean isValidLName(EditText editText) {
        return Validation.hasText(editText, getString(R.string.err_empty_lastname));
    }

    public boolean isValidFName(EditText editText) {
        return Validation.hasText(editText, getString(R.string.err_empty_firstname));
    }






}