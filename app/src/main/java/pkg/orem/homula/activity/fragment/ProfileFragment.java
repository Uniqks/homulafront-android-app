package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.utils.Utils;

public class ProfileFragment extends BaseFragment {


    private View mView;
    RelativeLayout changePassRL, faqRL, privacyRL;
    LinearLayout topLL;
    private ImageView mSaveImg, mEditImg, cameraIV, userIV;
    private EditText firstNameET, lastNameET, emailET, phoneNoET;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setActionBarCustom("", true);





        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        changePassRL = mView.findViewById(R.id.changePassRL);
        changePassRL.setOnClickListener(this);

        faqRL = mView.findViewById(R.id.faqRL);
        faqRL.setOnClickListener(this);

        privacyRL = mView.findViewById(R.id.privacyRL);
        privacyRL.setOnClickListener(this);

        topLL = mView.findViewById(R.id.topLL);
        baseActivity.setanimationView(topLL);


        mSaveImg = mView.findViewById(R.id.saveIV);
        mSaveImg.setOnClickListener(this);

        mEditImg = mView.findViewById(R.id.editIV);
        mEditImg.setOnClickListener(this);

        firstNameET = mView.findViewById(R.id.firstNameET);
        lastNameET = mView.findViewById(R.id.lastNameET);
        emailET = mView.findViewById(R.id.emailET);
        phoneNoET = mView.findViewById(R.id.phoneNoET);
        userIV = mView.findViewById(R.id.userIV);
        cameraIV = mView.findViewById(R.id.cameraIV);
        cameraIV.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.changePassRL:
                Utils.goToFragment(mContext, new ChangePasswordFragment(), R.id.fragment_container);
                break;

            case R.id.faqRL:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.privacyRL:
                Utils.goToFragment(mContext, new PrivacyFragment(), R.id.fragment_container);
                break;

            case R.id.editIV:
                mEditImg.setVisibility(View.GONE);
                mSaveImg.setVisibility(View.VISIBLE);
                cameraIV.setVisibility(View.VISIBLE);
                enableDisableFields(true);
                break;

            case R.id.saveIV:
                Toast.makeText(getActivity(), "Saved.", Toast.LENGTH_SHORT).show();
                mSaveImg.setVisibility(View.GONE);
                mEditImg.setVisibility(View.VISIBLE);
                cameraIV.setVisibility(View.GONE);
                enableDisableFields(false);
                break;

            case R.id.cameraIV:
                Toast.makeText(getActivity(), "work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void enableDisableFields(boolean is_enabled) {
        firstNameET.setEnabled(is_enabled);
        lastNameET.setEnabled(is_enabled);
        emailET.setEnabled(is_enabled);
        phoneNoET.setEnabled(is_enabled);
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

        }

        return super.onOptionsItemSelected(item); // important line
    }*/
}
