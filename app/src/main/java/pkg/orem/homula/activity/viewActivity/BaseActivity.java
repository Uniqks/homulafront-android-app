package pkg.orem.homula.activity.viewActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import pkg.orem.homula.R;
import pkg.orem.homula.activity.ForgotPwd;
import pkg.orem.homula.activity.Login;
import pkg.orem.homula.utils.PrefStore;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    public Context mContext;
    private Toolbar toolbar;
    private TextView titleTV;
    private ImageView backIV;
    public PrefStore store;
    public PermCallback permCallback;
    private int reqCode;
    private Dialog dialog;
    private TextView txtMsgTV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = BaseActivity.this;
        store = new PrefStore(this);
        progressDialog();
        // keyHash();
    }


    private void progressDialog() {
        dialog = new Dialog(this);
        View view = View.inflate(this, R.layout.progress_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txtMsgTV = (TextView) view.findViewById(R.id.txtMsgTV);
        dialog.setCancelable(false);
    }

    public void startProgressDialog() {
        if (dialog != null && !dialog.isShowing()) {
            try {
                dialog.show();
            } catch (Exception e) {
            }
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    public void stopProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    public String changeDateFormatFromDate(Date sourceDate, String targetDateFormat) {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        return outputDateFormat.format(sourceDate);
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseTime(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseHours(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "HH";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parsemin(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void keyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.orem.gallery.bazarapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:>>>>>>>>>>>>>>>", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
        }
    }

    @Override
    public void onClick(View v) {

    }

    public void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    public void log(String message) {
        Log.e(getString(R.string.app_name), message);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /*public void setToolbar(String title, boolean show) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        titleTV = (TextView) findViewById(R.id.titleTV);
        backIV = (ImageView) findViewById(R.id.backIV);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            titleTV.setText(title);
            if (show == false) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                backIV.setVisibility(View.GONE);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                backIV.setVisibility(View.VISIBLE);
            }

            backIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }*/


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @SuppressWarnings("deprecation")
    public boolean checkPermissions(String[] perms, int requestCode, PermCallback permCallback) {
        this.permCallback = permCallback;
        this.reqCode = requestCode;
        ArrayList<String> permsArray = new ArrayList<>();
        boolean hasPerms = true;
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                permsArray.add(perm);
                hasPerms = false;
            }
        }
        if (!hasPerms) {
            String[] permsString = new String[permsArray.size()];
            for (int i = 0; i < permsArray.size(); i++) {
                permsString[i] = permsArray.get(i);
            }
            ActivityCompat.requestPermissions(BaseActivity.this, permsString, 99);
            return false;
        } else
            return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permGrantedBool = false;
        switch (requestCode) {
            case 99:
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        showToast(getString(R.string.not_sufficient_permissions)
                                + getString(R.string.app_name)
                                + getString(R.string.permissionss));
                        permGrantedBool = false;
                        break;
                    } else {
                        permGrantedBool = true;
                    }
                }
                if (permCallback != null) {
                    if (permGrantedBool) {
                        permCallback.permGranted(reqCode);
                    } else {
                        permCallback.permDenied(reqCode);
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }


    public interface PermCallback {
        void permGranted(int resultCode);

        void permDenied(int resultCode);
    }

    public void gotoLoginActivity() {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();
    }

    /* public void gotoChooseSignUpActivity() {
        Intent intent = new Intent(this, ChooseSignupActivity.class);
        startActivity(intent);

    }*/
   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageUtils.activityResult(requestCode, resultCode, data);
    }  */

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageUtils.activityResult(requestCode, resultCode, data);
    }*/

    /* Description: Get Device token & Check device token is updated or not previous one
          Check whether we already login than it takes us on MainAgentActivity
          otherwise it takes us on login Screen
      * */
/*
    public void initFCM() {
        if (checkPlayServices()) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            if (refreshedToken == null) {
                if (store.getString(Conts.DEVICE_TOKEN) == null) {
                    if (store.getBoolean(Conts.IS_LOGIN) == true) {
                        if (store.getInt(Conts.CHECK_USER) == Conts.USER) {
                            gotoCustomerMainActivity();
                        } else {
                            gotoProviderMainActivity();
                        }
                    } else {
                        gotoCustomerLogin();
                    }

                } else {
                    store.saveString(Conts.DEVICE_TOKEN, refreshedToken);
                    if (store.getBoolean(Conts.IS_LOGIN) == true) {
                        if (store.getInt(Conts.CHECK_USER) == Conts.USER) {
                            gotoCustomerMainActivity();
                        } else {
                            gotoProviderMainActivity();
                        }
                    } else {
                        gotoCustomerLogin();
                    }

                }
            } else {
                if (!refreshedToken.equalsIgnoreCase(store.getString(Conts.DEVICE_TOKEN))) {
                    store.saveString(Conts.DEVICE_TOKEN, refreshedToken);
                    store.setBoolean(Conts.IS_LOGIN, false);
                    if (store.getBoolean(Conts.IS_LOGIN) == true) {
                        if (store.getInt(Conts.CHECK_USER) == Conts.USER) {
                            gotoCustomerMainActivity();
                        } else {
                            gotoProviderMainActivity();
                        }
                    } else {
                        gotoCustomerLogin();
                    }
                } else {
                    try {
                        if (refreshedToken != null) {
                            refreshedToken = new JSONObject(refreshedToken).getString(Conts.TOKEN);
                            store.saveString(Conts.DEVICE_TOKEN, refreshedToken);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (refreshedToken != null) {
                        store.saveString(Conts.DEVICE_TOKEN, refreshedToken);
                        if (store.getBoolean(Conts.IS_LOGIN) == true) {
                            if (store.getInt(Conts.CHECK_USER) == Conts.USER) {
                                gotoCustomerMainActivity();
                            } else {
                                gotoProviderMainActivity();
                            }
                        } else {
                            gotoCustomerLogin();
                        }

                    } else {
                        if (store.getString(Conts.DEVICE_TOKEN) == null) {
                            if (store.getBoolean(Conts.IS_LOGIN) == true) {
                                if (store.getInt(Conts.CHECK_USER) == Conts.USER) {
                                    gotoCustomerMainActivity();
                                } else {
                                    gotoProviderMainActivity();
                                }
                            } else {
                                gotoCustomerLogin();
                            }

                        } else {
                            if (store.getBoolean(Conts.IS_LOGIN) == true) {
                                if (store.getInt(Conts.CHECK_USER) == Conts.USER) {
                                    gotoCustomerMainActivity();
                                } else {
                                    gotoProviderMainActivity();
                                }
                            } else {
                                gotoCustomerLogin();
                            }

                        }
                    }
                }
            }
        }
    }

    */
    /*Through this method we check whether User's device is supportable for google services or not
     * @Return boolean-> "True" for Supportable for google services
     * "false" for unsupportable to google services*//*

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, Conts.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                showToast(getString(R.string.this_device_not_supported));
                finish();
            }
            return false;
        }
        return true;
    }
*/


    ///------------------------------------------------------LANGUAGE CHANGE---------------------------------------//


    public void checkLanguage() {
        String language = "English";
        try {
            language = store.getString("languages");
            if (language != null) {
                if (language.equals("Arabic")) {
                    String languageToLoad = "ar";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());

                } else if (language.equals("English")) {
                    String languageToLoad2 = "en";
                    Locale locale2 = new Locale(languageToLoad2);
                    Locale.setDefault(locale2);
                    Configuration config2 = new Configuration();
                    config2.locale = locale2;
                    getBaseContext().getResources().updateConfiguration(config2,
                            getBaseContext().getResources().getDisplayMetrics());
                } else if (language.equals("Kurdish")) {
                    String languageToLoad2 = "ar";
                    Locale locale2 = new Locale(languageToLoad2);
                    Locale.setDefault(locale2);
                    Configuration config2 = new Configuration();
                    config2.locale = locale2;
                    getBaseContext().getResources().updateConfiguration(config2,
                            getBaseContext().getResources().getDisplayMetrics());
                }
            } else {
                String languageToLoad2 = "ar";
                Locale locale2 = new Locale(languageToLoad2);
                Locale.setDefault(locale2);
                Configuration config2 = new Configuration();
                config2.locale = locale2;
                getBaseContext().getResources().updateConfiguration(config2,
                        getBaseContext().getResources().getDisplayMetrics());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
/*
    public void logout() {
        new ServerRequest<RegisterData>(mContext, Conts.logout(store.getString(Conts.USER_ID), store.getString(Conts.LANGUAGE)), true) {

            @Override
            public void onCompletion(Call<RegisterData> call, Response<RegisterData> response) {

                store.setBoolean(Conts.isRatingLogin, false);
                showToast(response.raw().message());
                store.setBoolean(Conts.ISFACEBOOKLOGIN, false);
                store.setBoolean(Conts.IS_LOGIN, false);
                //   store.setInt(Conts.CHECK_USER, Conts.USER);
                store.saveString(Conts.CITY, null);
                Intent intent = new Intent(BaseActivity.this, Splash.class);
                startActivity(intent);
                finish();

            }
        };
    }*/

    public static String formatValue(double value) {
        int power;
        String suffix = " kmbt";
        String formattedNumber = "";

        NumberFormat formatter = new DecimalFormat("#,###.#");
        power = (int) StrictMath.log10(value);
        value = value / (Math.pow(10, (power / 3) * 3));
        formattedNumber = formatter.format(value);
        formattedNumber = formattedNumber + suffix.charAt(power / 3);
        return formattedNumber.length() > 4 ? formattedNumber.replaceAll("\\.[0-9]+", "") : formattedNumber;
    }

}
