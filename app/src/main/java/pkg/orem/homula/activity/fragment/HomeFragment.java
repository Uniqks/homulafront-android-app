package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.activity.adapter.ViewPagerAdapter;

public class HomeFragment extends BaseFragment {

    private View mView;
    ViewPager viewPager;
    TabLayout tabLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setActionBarCustom("", true);





        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {
        viewPager = mView.findViewById(R.id.tabanim_viewpager);
        setupViewPager(viewPager);

        tabLayout = mView.findViewById(R.id.tabTL);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new HomeResidentalFragment(), "RESIDENTAL");
        adapter.addFrag(new HomeCommericalFragment(), "COMMERICAL");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.search) {
            Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item); // important line
    }
}
