package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.adapter.HomeResidentialAdapter;
import pkg.orem.homula.app.Constants;
import pkg.orem.homula.utils.Utils;

import java.util.ArrayList;

public class HomeResidentalFragment extends BaseFragment {


    private View mView;
    RecyclerView itemsRV;
    ArrayList<String> homeTopMusicData = new ArrayList<>();
    private LinearLayout topLL;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home_commerical, container, false);



        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        topLL = mView.findViewById(R.id.topLL);
        Animation animation1 =
                AnimationUtils.loadAnimation(baseActivity,
                        R.anim.fade);
        topLL.startAnimation(animation1);
        itemsRV = mView.findViewById(R.id.itemsRV);

        mView.findViewById(R.id.hotCW).setOnClickListener(this);
        mView.findViewById(R.id.allCW).setOnClickListener(this);
        mView.findViewById(R.id.exclusiveCW).setOnClickListener(this);
        mView.findViewById(R.id.comsoonCW).setOnClickListener(this);

        itemsRV.setHasFixedSize(true);
        itemsRV.setLayoutManager(new LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false));
        getData();

    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {

            case R.id.allCW:
                bundle.putString(Constants.mClass, "All");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;

            case R.id.hotCW:
                bundle.putString(Constants.mClass, "Hot");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;

            case R.id.exclusiveCW:
                bundle.putString(Constants.mClass, "Exclusive");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;

            case R.id.comsoonCW:
                bundle.putString(Constants.mClass, "Coming Soon");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;
        }
    }

    private void getData() {
        homeTopMusicData.clear();
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        HomeResidentialAdapter genresAdapter = new HomeResidentialAdapter(homeTopMusicData, baseActivity);
        itemsRV.setAdapter(genresAdapter);
    }
}
