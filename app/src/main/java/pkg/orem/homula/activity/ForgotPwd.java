package pkg.orem.homula.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.BaseActivity;
import pkg.orem.homula.app.HomulaApp;

public class ForgotPwd extends AppCompatActivity implements View.OnClickListener {


    private HomulaApp mHomulaApp;

    private LinearLayout topLL;
    private EditText mEmailEdt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mHomulaApp = (HomulaApp) getApplication();




        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        topLL = findViewById(R.id.topLL);
        topLL.setVisibility(View.GONE);
        setanimationView(topLL);

        mEmailEdt = findViewById(R.id.edt_email);

        findViewById(R.id.img_back).setOnClickListener(this);
        findViewById(R.id.btn_send).setOnClickListener(this);
    }


    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mHomulaApp.isValidEmail(mEmailEdt)) {
            return false;
        }

        /*if (Utils.isNetworkAvailable(Login.this)) {
           // doLoginService();
        } else {
            Utils.makeToast(getString(R.string.no_internet_connection), Login.this);
        }*/
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back:
                mHomulaApp.hideKeyboard(ForgotPwd.this);
                finish();
                break;

            case R.id.btn_send:
                mHomulaApp.hideKeyboard(ForgotPwd.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.btn_send));

                if (checkValidation()){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 200);
                }
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
