package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.utils.Utils;

public class ProfessionalFinderTwoFragment extends BaseFragment {
    private View view;
    LinearLayout topLL;
    CardView   priceCW,  specialCW,  fiveCW, oneCW, twoCW, aboveCW;
    Button nextBT;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_professional_finder_two, container, false);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.proffessional_finder), true);
        init(view);
        return view;
    }

    private void init(View view) {
        priceCW = view.findViewById(R.id.priceCW);
        topLL = view.findViewById(R.id.topLL);
        aboveCW = view.findViewById(R.id.aboveCW);
        oneCW = view.findViewById(R.id.oneCW);
        twoCW = view.findViewById(R.id.twoCW);
        fiveCW = view.findViewById(R.id.fiveCW);
        specialCW = view.findViewById(R.id.specialCW);
        nextBT = view.findViewById(R.id.nextBT);
        fiveCW.setOnClickListener(this);
        twoCW.setOnClickListener(this);
        oneCW.setOnClickListener(this);
        aboveCW.setOnClickListener(this);
        nextBT.setOnClickListener(this);
        baseActivity.setanimationView(topLL);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fiveCW:
                setSpecialLayout();
                break;
            case R.id.oneCW:
                setSpecialLayout();
                break;
            case R.id.twoCW:
                setSpecialLayout();
                break;
            case R.id.aboveCW:
                setSpecialLayout();
                break;
            case R.id.nextBT:
                Utils.goToFragment(mContext, new ContactMeFragment(), R.id.fragment_container);
                break;
        }
    }

    private void setSpecialLayout() {
        priceCW.setVisibility(View.GONE);
        nextBT.setVisibility(View.VISIBLE);
        specialCW.setVisibility(View.VISIBLE);
        baseActivity.setanimationView(specialCW);
    }

}
