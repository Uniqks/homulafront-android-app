package pkg.orem.homula.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import pkg.orem.homula.activity.viewActivity.BaseActivity;
import pkg.orem.homula.activity.viewActivity.MainActivity;


public class BaseFragment extends Fragment implements View.OnClickListener {

    Context mContext;
    public BaseActivity baseActivity;
    public MainActivity mainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        baseActivity = (BaseActivity) getActivity();

        if (mainActivity == null) {
            mainActivity = (MainActivity) getActivity();
        }
    }


    public void setToolbarIcon(boolean isEditIcon, boolean isSaveIcon) {



    }

    @Override
    public void onClick(View v) {

    }

    public void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
}
