package pkg.orem.homula.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;

import java.io.File;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.BaseActivity;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.app.HomulaApp;
import pkg.orem.homula.utils.Utils;

public class Register extends AppCompatActivity implements View.OnClickListener {


    private HomulaApp mHomulaApp;

    private LinearLayout topLL;
    private CheckBox mTermsCheck;
    private EditText mFnameEdt, mLnameEdt, mEmailEdt, mPhoneEdt, mPwdEdt, mCpwdEdt;

    private ImageView mProImg;
    public static final int SELECT_PHOTO = 1;
    public static final int TAKENPHOTO = 0;
    public String photo_path = "";

    Uri uri;
    File photofile;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private int STORAGE_PERMISSION_CODE = 11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mHomulaApp = (HomulaApp) getApplication();




        initControls();
    }


    /**
     * Method is used to initialized all the controls....
     */
    private void initControls() {

        topLL = findViewById(R.id.topLL);
        topLL.setVisibility(View.GONE);
        setanimationView(topLL);

        mTermsCheck = findViewById(R.id.check_terms);
        String read = getString(R.string.by_clicking_this_box);
        mTermsCheck.setText(Html.fromHtml(read));

        mFnameEdt = findViewById(R.id.edt_fname);
        mLnameEdt = findViewById(R.id.edt_lname);
        mEmailEdt = findViewById(R.id.edt_email);
        mPhoneEdt = findViewById(R.id.edt_phone);
        mPwdEdt = findViewById(R.id.edt_pwd);
        mCpwdEdt = findViewById(R.id.edt_cpwd);


        findViewById(R.id.img_back).setOnClickListener(this);
        findViewById(R.id.btn_register).setOnClickListener(this);
        findViewById(R.id.ll_login).setOnClickListener(this);

        mProImg = findViewById(R.id.img_pro);
        findViewById(R.id.img_camera).setOnClickListener(this);
    }


    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {

        if (!mHomulaApp.isValidFName(mFnameEdt)) {
            return false;
        }
        if (!mHomulaApp.isValidLName(mLnameEdt)) {
            return false;
        }
        if (!mHomulaApp.isValidEmail(mEmailEdt)) {
            return false;
        }
        if (!mHomulaApp.isValidPhone(mPhoneEdt)) {
            return false;
        }
        if (!mHomulaApp.isValidatePassword(mPwdEdt)) {
            return false;
        }
        if (!mHomulaApp.isValidConfirmPassword(mPwdEdt, mCpwdEdt)) {
            return false;
        }
        if (!mTermsCheck.isChecked()) {
            Toast.makeText(Register.this, getString(R.string.err_accept_terms_conditions), Toast.LENGTH_SHORT).show();
            return false;
        }
        /*if (Utils.isNetworkAvailable(Login.this)) {
           // doLoginService();
        } else {
            Utils.makeToast(getString(R.string.no_internet_connection), Login.this);
        }*/
        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back:
                mHomulaApp.hideKeyboard(Register.this);
                finish();
                break;

            case R.id.btn_register:
                mHomulaApp.hideKeyboard(Register.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.btn_register));

                if (checkValidation()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Register.this, MainActivity.class));
                            finish();
                        }
                    }, 200);
                }
                break;

            case R.id.ll_login:
                mHomulaApp.hideKeyboard(Register.this);
                startActivity(new Intent(Register.this, Login.class));
                finish();
                break;

            case R.id.img_camera:
                mHomulaApp.hideKeyboard(Register.this);
                if (!hasPermissions(Register.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(Register.this, PERMISSIONS, STORAGE_PERMISSION_CODE);
                } else {
                    Toast.makeText(Register.this, "work in progress...", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    /**
     *  multimedia permission...
     */
    public static boolean hasPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     *  Method is used to initialized selcet image...
     */
    public void showPopup() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.str_select_one));
        builder.setMessage(getString(R.string.str_select_gallery_msg));
        builder.setNegativeButton(getString(R.string.str_camera).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                takePhotoFromCamera();
            }
        });
        builder.setPositiveButton(getString(R.string.str_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), SELECT_PHOTO);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void takePhotoFromCamera() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            photofile = Utils.getInstance(this).create_file(".jpg");
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            uri = FileProvider.getUriForFile(this, "dev.env.potriders.fileprovider", photofile);
        } else {
            photofile = Utils.getInstance(this).create_Image_File(".jpg");
            uri = Uri.fromFile(photofile);

        }
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(Intent.createChooser(cameraIntent, getString(R.string.lbl_capture)), TAKENPHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == TAKENPHOTO) && (resultCode == Activity.RESULT_OK)) {

            if (uri != null) {

                photo_path = photofile.getAbsolutePath();//uri.getPath();

                System.out.println("image path -->" + uri.getPath() + " imageFilePath " + new File(uri.toString()).getAbsolutePath());
                if (photo_path != null && photo_path.length() > 0) {

                    Picasso.get()
                            .load(new File(photo_path))
                            .into(mProImg);
                }
            } else {
                Utils.getInstance(this).Toast(this, getString(R.string.toast_unable_to_selct_image), false);
            }

        } else if ((requestCode == SELECT_PHOTO) && (resultCode == Activity.RESULT_OK)) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            photo_path = cursor.getString(columnIndex);
            try {
                System.out.println("select photo ---> " + photo_path);

                /*Glide.with(this)
                        .load(new File(photo_path))
                        .asBitmap()
                        .centerCrop()
                        .into(new BitmapImageViewTarget(mProImg) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mProImg.setImageDrawable(circularBitmapDrawable);
                            }
                        });*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (photo_path != null && !photo_path.equals(""))

            System.out.println("register --->" +  " onActivityResult photo_path " + photo_path);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setanimationView(topLL);
    }

    /**
     *  Method is used to set animation ...
     */
    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(Register.this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
