package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.utils.Utils;

public class FilterFragment extends BaseFragment {


    private View mView;
    TextView amenitiesTV, bathroomTV, bedroomTV, priceTV, typeTV, loctionTV;
    LinearLayout bedRoomLL, priceLL, amitiesLL, typeLL, locationLL;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_filter, container, false);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.filter), true);



        initControls();
        return mView;
    }

    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {

        loctionTV = mView.findViewById(R.id.loctionTV);
        typeTV = mView.findViewById(R.id.typeTV);
        priceTV = mView.findViewById(R.id.priceTV);
        bedroomTV = mView.findViewById(R.id.bedroomTV);
        bathroomTV = mView.findViewById(R.id.bathroomTV);
        amenitiesTV = mView.findViewById(R.id.amenitiesTV);

        locationLL = mView.findViewById(R.id.locationLL);
        typeLL = mView.findViewById(R.id.typeLL);
        amitiesLL = mView.findViewById(R.id.amitiesLL);
        priceLL = mView.findViewById(R.id.priceLL);
        bedRoomLL = mView.findViewById(R.id.bedRoomLL);

        amenitiesTV.setOnClickListener(this);
        bathroomTV.setOnClickListener(this);
        bedroomTV.setOnClickListener(this);
        priceTV.setOnClickListener(this);
        typeTV.setOnClickListener(this);
        loctionTV.setOnClickListener(this);

        mView.findViewById(R.id.txt_reset).setOnClickListener(this);
        mView.findViewById(R.id.txt_apply).setOnClickListener(this);

        setLayout(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.loctionTV:
                setLayout(0);
                break;

            case R.id.typeTV:
                setLayout(1);
                break;

            case R.id.priceTV:
                setLayout(2);
                break;

            case R.id.bedroomTV:
                setLayout(3);
                break;

            case R.id.bathroomTV:
                setLayout(4);
                break;

            case R.id.amenitiesTV:
                setLayout(5);
                break;

            case R.id.txt_reset:
                Toast.makeText(getActivity(), "reset all data.", Toast.LENGTH_SHORT).show();
                break;

            case R.id.txt_apply:
                Toast.makeText(getActivity(), "work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void setLayout(int i) {
        loctionTV.setBackgroundColor(getResources().getColor(R.color.veryLightGray));
        typeTV.setBackgroundColor(getResources().getColor(R.color.veryLightGray));
        priceTV.setBackgroundColor(getResources().getColor(R.color.veryLightGray));
        bedroomTV.setBackgroundColor(getResources().getColor(R.color.veryLightGray));
        bathroomTV.setBackgroundColor(getResources().getColor(R.color.veryLightGray));
        amenitiesTV.setBackgroundColor(getResources().getColor(R.color.veryLightGray));

        locationLL.setVisibility(View.GONE);
        typeLL.setVisibility(View.GONE);
        amitiesLL.setVisibility(View.GONE);
        priceLL.setVisibility(View.GONE);
        bedRoomLL.setVisibility(View.GONE);

        if (i == 0) {
            loctionTV.setBackgroundColor(getResources().getColor(R.color.white));
            locationLL.setVisibility(View.VISIBLE);
            baseActivity.setanimationView(locationLL);
        } else if (i == 1) {
            typeTV.setBackgroundColor(getResources().getColor(R.color.white));
            typeLL.setVisibility(View.VISIBLE);
            baseActivity.setanimationView(typeLL);
        } else if (i == 2) {
            priceTV.setBackgroundColor(getResources().getColor(R.color.white));
            priceLL.setVisibility(View.VISIBLE);
            baseActivity.setanimationView(priceLL);
        } else if (i == 3) {
            bedroomTV.setBackgroundColor(getResources().getColor(R.color.white));
            bedRoomLL.setVisibility(View.VISIBLE);
            baseActivity.setanimationView(bedRoomLL);
        } else if (i == 4) {
            bathroomTV.setBackgroundColor(getResources().getColor(R.color.white));
            bedRoomLL.setVisibility(View.VISIBLE);
            baseActivity.setanimationView(bedRoomLL);
        } else {
            amenitiesTV.setBackgroundColor(getResources().getColor(R.color.white));
            amitiesLL.setVisibility(View.VISIBLE);
            baseActivity.setanimationView(amitiesLL);
        }
    }
}
