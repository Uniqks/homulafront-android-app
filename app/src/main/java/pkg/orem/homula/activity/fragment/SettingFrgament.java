package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;


public class SettingFrgament extends BaseFragment {
    private View view;
    RelativeLayout contactUsRL, aboutUsRL, privacyPolicyRL;
    LinearLayout topLL;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.settings), true);
        init(view);
        return view;
    }

    private void init(View view) {
        contactUsRL = view.findViewById(R.id.contactUsRL);
        topLL = view.findViewById(R.id.topLL);
        aboutUsRL = view.findViewById(R.id.aboutUsRL);
        privacyPolicyRL = view.findViewById(R.id.privacyPolicyRL);
        privacyPolicyRL.setOnClickListener(this);
        aboutUsRL.setOnClickListener(this);
        contactUsRL.setOnClickListener(this);
        baseActivity.setanimationView(topLL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contactUsRL:
              //  Utils.goToFragment(mContext, new ContactUsFragment(), R.id.fragment_container);
                break;
            case R.id.aboutUsRL:

                break;
            case R.id.privacyPolicyRL:

                break;
        }
    }
}
