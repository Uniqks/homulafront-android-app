package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.adapter.HomeCommericalAdapter;
import pkg.orem.homula.app.Constants;
import pkg.orem.homula.utils.Utils;

import java.util.ArrayList;

public class HomeCommericalFragment extends BaseFragment {

    private View mView;
    RecyclerView itemsRV;
    ArrayList<String> homeTopMusicData = new ArrayList<>();
    LinearLayout topLL;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home_commerical, container, false);




        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mView.findViewById(R.id.hotCW).setOnClickListener(this);
        mView.findViewById(R.id.allCW).setOnClickListener(this);
        mView.findViewById(R.id.exclusiveCW).setOnClickListener(this);
        mView.findViewById(R.id.comsoonCW).setOnClickListener(this);

        itemsRV = mView.findViewById(R.id.itemsRV);
        itemsRV.setHasFixedSize(true);
        itemsRV.setLayoutManager(new LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false));
        getData();
    }

    private void getData() {
        homeTopMusicData.clear();
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        HomeCommericalAdapter genresAdapter = new HomeCommericalAdapter(homeTopMusicData, baseActivity);
        itemsRV.setAdapter(genresAdapter);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {

            case R.id.allCW:
                bundle.putString(Constants.mClass, "All");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;

            case R.id.hotCW:
                bundle.putString(Constants.mClass, "Hot");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;

            case R.id.exclusiveCW:
                bundle.putString(Constants.mClass, "Exclusive");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;

            case R.id.comsoonCW:
                bundle.putString(Constants.mClass, "Coming Soon");
                Utils.goToBundleFragment(mContext, new AllFragment(), R.id.fragment_container, bundle);
                break;
        }
    }

}
