package pkg.orem.homula.activity.viewActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.Login;
import pkg.orem.homula.activity.fragment.AIFragment;
import pkg.orem.homula.activity.fragment.CamparePropertyFragment;
import pkg.orem.homula.activity.fragment.FavFragment;
import pkg.orem.homula.activity.fragment.HelpCenterFragment;
import pkg.orem.homula.activity.fragment.HomeFragment;
import pkg.orem.homula.activity.fragment.LocalRealtorsFragment;
import pkg.orem.homula.activity.fragment.MortgageCalculatorFragment;
import pkg.orem.homula.activity.fragment.ProfessionalFinderFragment;
import pkg.orem.homula.activity.fragment.ProfileFragment;
import pkg.orem.homula.activity.fragment.SearchFragment;
import pkg.orem.homula.activity.fragment.SettingFrgament;
import pkg.orem.homula.utils.Utils;

public class MainActivity extends BaseActivity {


    LinearLayout homeActionLL, favLL, viewProfileLL, homeDraLL, helpCenterLL, professionalLL, mySavedSearchLL, settingLL, logoutLL,
            liveSearchLL, ontarioSearchLL, canadaSearchLL, newCandoSearchLL, internationalSearchLL, martgageLL, compareLL, favDraLL,
            profileLL, homeLL, localLL, searchOptionLL, toolsOptionLL;

    ImageView profileIV, favIV, searchIV, closeDrIV, homeIV, toolIV, localIV, backIV, menuIV;

    public ImageView editIV, saveIV;

    TextView profileTV, favTV, homeTV, localTV, titleTV;

    RelativeLayout drawerRL, toolRL, searchRL;
    CardView belowLL;
    boolean isSearch, isTool, isOpen;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






        initControls();
    }


    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        toolsOptionLL = findViewById(R.id.toolsOptionLL);
        searchOptionLL = findViewById(R.id.searchOptionLL);

        editIV = findViewById(R.id.editIV);
        saveIV = findViewById(R.id.saveIV);

        //drawer
        drawerRL = findViewById(R.id.drawerRL);
        drawerRL.setOnClickListener(this);

        homeDraLL = findViewById(R.id.homeDraLL);
        homeDraLL.setOnClickListener(this);

        searchRL = findViewById(R.id.searchRL);
        searchRL.setOnClickListener(this);
        searchIV = findViewById(R.id.searchIV);
        searchIV.setBackgroundResource(R.drawable.down);

        liveSearchLL = findViewById(R.id.liveSearchLL);
        liveSearchLL.setOnClickListener(this);
        ontarioSearchLL = findViewById(R.id.ontarioSearchLL);
        ontarioSearchLL.setOnClickListener(this);
        canadaSearchLL = findViewById(R.id.canadaSearchLL);
        canadaSearchLL.setOnClickListener(this);
        newCandoSearchLL = findViewById(R.id.newCandoSearchLL);
        newCandoSearchLL.setOnClickListener(this);
        internationalSearchLL = findViewById(R.id.internationalSearchLL);
        internationalSearchLL.setOnClickListener(this);

        compareLL = findViewById(R.id.compareLL);
        compareLL.setOnClickListener(this);

        favDraLL = findViewById(R.id.favDraLL);
        favDraLL.setOnClickListener(this);

        toolRL = findViewById(R.id.toolRL);
        toolRL.setOnClickListener(this);
        toolIV = findViewById(R.id.toolIV);
        toolIV.setBackgroundResource(R.drawable.down);

        martgageLL = findViewById(R.id.martgageLL);
        martgageLL.setOnClickListener(this);
        professionalLL = findViewById(R.id.professionalLL);
        professionalLL.setOnClickListener(this);
        mySavedSearchLL = findViewById(R.id.mySavedSearchLL);
        mySavedSearchLL.setOnClickListener(this);

        helpCenterLL = findViewById(R.id.helpCenterLL);
        helpCenterLL.setOnClickListener(this);

        settingLL = findViewById(R.id.settingLL);
        settingLL.setOnClickListener(this);

        logoutLL = findViewById(R.id.logoutLL);
        logoutLL.setOnClickListener(this);

        viewProfileLL = findViewById(R.id.viewProfileLL);
        viewProfileLL.setOnClickListener(this);

        //bottom
        homeLL = findViewById(R.id.homeLL);
        homeLL.setOnClickListener(this);
        homeIV = findViewById(R.id.homeIV);
        homeTV = findViewById(R.id.homeTV);

        localLL = findViewById(R.id.localLL);
        localLL.setOnClickListener(this);
        localIV = findViewById(R.id.localIV);
        localTV = findViewById(R.id.localTV);

        favLL = findViewById(R.id.favLL);
        favLL.setOnClickListener(this);
        favIV = findViewById(R.id.favIV);
        favTV = findViewById(R.id.favTV);

        profileLL = findViewById(R.id.profileLL);
        profileLL.setOnClickListener(this);
        profileIV = findViewById(R.id.profileIV);
        profileTV = findViewById(R.id.profileTV);


        closeDrIV = findViewById(R.id.closeDrIV);
        closeDrIV.setOnClickListener(this);

        gotoHome(false);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.drawerRL:
                openAndCloseDrawer();
                break;

            case R.id.homeDraLL:
                gotoHome(true);
                break;

            case R.id.searchRL:
                if (isSearch) {
                    searchIV.setBackgroundResource(R.drawable.down);
                    searchOptionLL.setVisibility(View.GONE);
                    isSearch = false;
                } else {
                    searchIV.setBackgroundResource(R.drawable.up);
                    searchOptionLL.setVisibility(View.VISIBLE);

                    isSearch = true;
                }
                break;

            case R.id.compareLL:
                openAndCloseDrawer();
                Utils.goToFragment(mContext, new CamparePropertyFragment(), R.id.fragment_container);
                break;

            case R.id.favDraLL:
                gotoFav(true);
                break;

            case R.id.toolRL:
                if (isTool) {
                    toolIV.setBackgroundResource(R.drawable.down);
                    toolsOptionLL.setVisibility(View.GONE);
                    isTool = false;
                } else {
                    toolIV.setBackgroundResource(R.drawable.up);
                    toolsOptionLL.setVisibility(View.VISIBLE);
                    isTool = true;
                }
                break;

            case R.id.helpCenterLL:
                openAndCloseDrawer();
                Utils.goToFragment(mContext, new HelpCenterFragment(), R.id.fragment_container);
                break;

            case R.id.logoutLL:
                openAndCloseDrawer();
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(MainActivity.this);
                }
                builder.setTitle("Please Confirm");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(MainActivity.this, Login.class));
                        finish();
                    }
                })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;

            case R.id.liveSearchLL:
                openAndCloseDrawer();
                Utils.goToFragment(mContext, new SearchFragment(), R.id.fragment_container);
                break;

            case R.id.ontarioSearchLL:
                openAndCloseDrawer();
                Toast.makeText(MainActivity.this, "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.canadaSearchLL:
                openAndCloseDrawer();
                Toast.makeText(MainActivity.this, "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.newCandoSearchLL:
                openAndCloseDrawer();
                Toast.makeText(MainActivity.this, "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.internationalSearchLL:
                openAndCloseDrawer();
                Toast.makeText(MainActivity.this, "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.martgageLL:
                openAndCloseDrawer();
                Utils.goToFragment(mContext, new MortgageCalculatorFragment(), R.id.fragment_container);
                break;

            case R.id.professionalLL:
                openAndCloseDrawer();
                Utils.goToFragment(mContext, new ProfessionalFinderFragment(), R.id.fragment_container);
                break;

            case R.id.mySavedSearchLL:
                openAndCloseDrawer();
                Toast.makeText(MainActivity.this, "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.viewProfileLL:
                gotoProfile();
                break;

            case R.id.homeLL:
                gotoHome(false);
                break;

            case R.id.localLL:
                gotoLocal(false);
                break;

            case R.id.favLL:
                gotoFav(false);
                break;

            case R.id.profileLL:
                gotoAI(false);
                break;

            case R.id.settingLL:
                openAndCloseDrawer();
                Utils.goToFragment(mContext, new SettingFrgament(), R.id.fragment_container);
                break;
        }
    }


    /**
     * Method is used to initialized Home data...
     */
    private void gotoHome(boolean drawer) {
        if (drawer) {
            openAndCloseDrawer();
        }
        homeTV.setTextColor(getResources().getColor(R.color.blue));
        localTV.setTextColor(getResources().getColor(R.color.green));
        favTV.setTextColor(getResources().getColor(R.color.green));
        profileTV.setTextColor(getResources().getColor(R.color.green));

        homeIV.setBackgroundResource(R.drawable.home_normal);
        localIV.setBackgroundResource(R.drawable.local_realtors_selected);
        favIV.setBackgroundResource(R.drawable.my_favourites_selected);
        profileIV.setBackgroundResource(R.drawable.ai_selected);
        Utils.goToFragment(mContext, new HomeFragment(), R.id.fragment_container);
    }

    /**
     *  Method is used to initialized Local Realtors...
     */
    private void gotoLocal(boolean drawer) {
        if (drawer) {
            openAndCloseDrawer();
        }
        homeTV.setTextColor(getResources().getColor(R.color.green));
        localTV.setTextColor(getResources().getColor(R.color.blue));
        favTV.setTextColor(getResources().getColor(R.color.green));
        profileTV.setTextColor(getResources().getColor(R.color.green));

        homeIV.setBackgroundResource(R.drawable.home_selected);
        localIV.setBackgroundResource(R.drawable.local_realtors_normal);
        favIV.setBackgroundResource(R.drawable.my_favourites_selected);
        profileIV.setBackgroundResource(R.drawable.ai_selected);
        Utils.goToFragment(mContext, new LocalRealtorsFragment(), R.id.fragment_container);
    }

    /**
     *  Method is used to initialized favourites..
     */
    private void gotoFav(boolean drawer) {
        if (drawer) {
            openAndCloseDrawer();
        }
        homeTV.setTextColor(getResources().getColor(R.color.green));
        localTV.setTextColor(getResources().getColor(R.color.green));
        favTV.setTextColor(getResources().getColor(R.color.blue));
        profileTV.setTextColor(getResources().getColor(R.color.green));

        homeIV.setBackgroundResource(R.drawable.home_selected);
        localIV.setBackgroundResource(R.drawable.local_realtors_selected);
        favIV.setBackgroundResource(R.drawable.my_favourites_normal);
        profileIV.setBackgroundResource(R.drawable.ai_selected);
        Utils.goToFragment(mContext, new FavFragment(), R.id.fragment_container);
    }

    /**
     *  Method is used to initialized AI(chat)..
     */
    private void gotoAI(boolean drawer) {
        if (drawer) {
            openAndCloseDrawer();
        }
        localTV.setTextColor(getResources().getColor(R.color.green));
        homeTV.setTextColor(getResources().getColor(R.color.green));
        profileTV.setTextColor(getResources().getColor(R.color.blue));
        favTV.setTextColor(getResources().getColor(R.color.green));

        localIV.setBackgroundResource(R.drawable.local_realtors_selected);
        homeIV.setBackgroundResource(R.drawable.home_selected);
        profileIV.setBackgroundResource(R.drawable.ai_normal);
        favIV.setBackgroundResource(R.drawable.my_favourites_selected);
        Utils.goToFragment(mContext, new AIFragment(), R.id.fragment_container);
    }

    /**
     *  Method is used to initialized view user Profile...
     */
    private void gotoProfile() {
        openAndCloseDrawer();
        Utils.goToFragment(mContext, new ProfileFragment(), R.id.fragment_container);
    }


    /**
     *  Method is used to set custom actionbar....
     */
    public void setActionBarCustom(String title, boolean show) {

        titleTV = findViewById(R.id.titleTV);
        backIV = findViewById(R.id.backIV);
        menuIV = findViewById(R.id.menuIV);

        homeActionLL = findViewById(R.id.homeActionLL);
        belowLL = findViewById(R.id.belowLL);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof HomeFragment
                || fragment instanceof ProfileFragment
                || fragment instanceof FavFragment
                || fragment instanceof LocalRealtorsFragment
                || fragment instanceof AIFragment) {
            belowLL.setVisibility(View.VISIBLE);
            menuIV.setVisibility(View.VISIBLE);
            backIV.setVisibility(View.GONE);
            homeActionLL.setVisibility(View.VISIBLE);
            titleTV.setVisibility(View.GONE);
        } else if (fragment instanceof SearchFragment
                || fragment instanceof ProfessionalFinderFragment
                || fragment instanceof CamparePropertyFragment
                || fragment instanceof SettingFrgament) {
            belowLL.setVisibility(View.VISIBLE);
            menuIV.setVisibility(View.VISIBLE);
            backIV.setVisibility(View.GONE);
            homeActionLL.setVisibility(View.GONE);
            titleTV.setVisibility(View.VISIBLE);
        } else {
            menuIV.setVisibility(View.GONE);
            belowLL.setVisibility(View.GONE);
            homeActionLL.setVisibility(View.GONE);
            titleTV.setVisibility(View.VISIBLE);
            backIV.setVisibility(View.VISIBLE);
        }
        if (fragment instanceof SearchFragment
                || fragment instanceof SettingFrgament
                || fragment instanceof ProfessionalFinderFragment
                || fragment instanceof CamparePropertyFragment) {
            belowLL.setVisibility(View.GONE);
        }
        titleTV.setText(title);
        if (show == true) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
        }

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                onBackPressed();
            }
        });
        menuIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAndCloseDrawer();
            }
        });
    }


    /**
     *  Method is used to initialized open & close drawer....
     */
    private void openAndCloseDrawer() {
        if (isOpen) {
            isOpen = false;
            Animation rotation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_45);
            // rotation.setFillAfter(true);
            drawerRL.startAnimation(rotation);
            drawerRL.setVisibility(View.GONE);
            rotation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    drawerRL.setVisibility(View.GONE);
                    searchIV.setBackgroundResource(R.drawable.down);
                    searchOptionLL.setVisibility(View.GONE);
                    isSearch = false;
                    toolIV.setBackgroundResource(R.drawable.down);
                    toolsOptionLL.setVisibility(View.GONE);
                    isTool = false;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    drawerRL.setVisibility(View.GONE);
                    searchIV.setBackgroundResource(R.drawable.down);
                    searchOptionLL.setVisibility(View.GONE);
                    isSearch = false;
                    toolIV.setBackgroundResource(R.drawable.down);
                    toolsOptionLL.setVisibility(View.GONE);
                    isTool = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            isOpen = true;
            Animation rotation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_0);
           // rotation.setFillAfter(true);
            drawerRL.startAnimation(rotation);
            drawerRL.setVisibility(View.VISIBLE);
            rotation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    drawerRL.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    drawerRL.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

        }

    }


    @Override
    public void onBackPressed() {

        if (isOpen) {
            openAndCloseDrawer();
        } else if (getFragmentManager().getBackStackEntryCount()>0) {
        }


        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof HomeFragment) {

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(MainActivity.this);
            }
            builder.setTitle("Please Confirm");
            builder.setMessage("Are you sure you want to exit?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else if (fragment instanceof ProfileFragment
                || fragment instanceof LocalRealtorsFragment
                || fragment instanceof FavFragment
                || fragment instanceof SearchFragment
                || fragment instanceof CamparePropertyFragment
                || fragment instanceof ProfessionalFinderFragment
                || fragment instanceof AIFragment
                || fragment instanceof SettingFrgament
                ) {
            gotoHome(false);
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
