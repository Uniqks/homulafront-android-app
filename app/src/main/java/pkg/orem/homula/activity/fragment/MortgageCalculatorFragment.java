package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;

public class MortgageCalculatorFragment extends BaseFragment {
    private View view;
    RelativeLayout topLL;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mortgage_calculator, container, false);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.martgage_calculator), true);
        init(view);
        return view;
    }

    private void init(View view) {

        topLL = view.findViewById(R.id.topLL);
        baseActivity.setanimationView(topLL);
    }
}
