package pkg.orem.homula.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.BaseActivity;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.app.HomulaApp;
import pkg.orem.homula.utils.Utils;

public class Login extends AppCompatActivity implements View.OnClickListener {


    private HomulaApp mHomulaApp;

    private LinearLayout topLL;
    private EditText mEmailEdt, mPwdEdt;

    private SharedPreferences mRememberMePref;
    private SharedPreferences.Editor mEditor;
    boolean saveLogin;
    private CheckBox mRememberChk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mHomulaApp = (HomulaApp) getApplication();





        initControls();
        mRememberMePref = getSharedPreferences("RememberMePref", 0);
        mEditor = mRememberMePref.edit();
        saveLogin = mRememberMePref.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            mEmailEdt.setText(mRememberMePref.getString("username", ""));
            mPwdEdt.setText(mRememberMePref.getString("password", ""));
            mRememberChk.setChecked(true);
        }
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        topLL = findViewById(R.id.topLL);
        topLL.setVisibility(View.GONE);
        setanimationView(topLL);


        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.ll_register).setOnClickListener(this);
        findViewById(R.id.txt_fpwd).setOnClickListener(this);

        mEmailEdt = findViewById(R.id.edt_email);
        mPwdEdt = findViewById(R.id.edt_pwd);

        mRememberChk = findViewById(R.id.rememberCB);
    }

    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mHomulaApp.isValidEmail(mEmailEdt)) {
            return false;
        }
        if (!mHomulaApp.isValidatePassword(mPwdEdt)) {
            return false;
        }
        /*if (Utils.isNetworkAvailable(Login.this)) {
           // doLoginService();
        } else {
            Utils.makeToast(getString(R.string.no_internet_connection), Login.this);
        }*/
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                mHomulaApp.hideKeyboard(Login.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.btn_login));

                if (checkValidation()){

                    if (mRememberChk.isChecked()) {
                        mEditor.putBoolean("saveLogin", true);
                        mEditor.putString("username", mEmailEdt.getText().toString());
                        mEditor.putString("password", mPwdEdt.getText().toString());
                        mEditor.commit();
                    } else {
                        mEditor.clear();
                        mEditor.commit();
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Login.this, MainActivity.class));
                            finish();
                        }
                    }, 200);
                }
                break;

            case R.id.txt_fpwd:
                mHomulaApp.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, ForgotPwd.class));
                break;

            case R.id.ll_register:
                mHomulaApp.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, Register.class));
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setanimationView(topLL);
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }
}
