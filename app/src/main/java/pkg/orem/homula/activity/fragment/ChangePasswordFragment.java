package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;

public class ChangePasswordFragment extends BaseFragment {


    private View mView;
    ScrollView topLL;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_change_password, container, false);
        ((MainActivity) getActivity()).setActionBarCustom("", false);





        initControls();
        return mView;
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        topLL = mView.findViewById(R.id.topLL);
        baseActivity.setanimationView(topLL);

        mView.findViewById(R.id.btn_done).setOnClickListener(this);
        mView.findViewById(R.id.backIV).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.backIV:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.btn_done:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
