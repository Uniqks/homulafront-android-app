package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;

public class ComparsionDetailFragment extends BaseFragment {


    private View mView;
    RelativeLayout topLL;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_comparsion_detail, container, false);
        ((MainActivity) getActivity()).setActionBarCustom("comparison Detail", true);




        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {
        topLL = mView.findViewById(R.id.topLL);
        baseActivity.setanimationView(topLL);
    }
}
