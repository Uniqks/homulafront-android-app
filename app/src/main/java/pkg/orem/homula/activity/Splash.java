package pkg.orem.homula.activity;

import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.BaseActivity;


public class Splash extends BaseActivity {


    private ImageView logoIV, bottomIV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);




        initControls();
        slideDown();
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        logoIV = findViewById(R.id.logoIV);
        bottomIV = findViewById(R.id.bottomIV);
    }


    private void slideDown() {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        bottomIV.startAnimation(bottomUp);
        bottomIV.setVisibility(View.VISIBLE);
        slideUp();
    }

    private void slideUp() {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        logoIV.startAnimation(bottomUp);
        logoIV.setVisibility(View.VISIBLE);
        setHandler();
    }

    private void setHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoLoginActivity();
            }
        }, 2000);
    }

}
