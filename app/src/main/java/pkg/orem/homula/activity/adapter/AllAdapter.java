package pkg.orem.homula.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.fragment.ResidentialDetailsFragment;
import pkg.orem.homula.utils.Utils;

import java.util.ArrayList;

public class AllAdapter extends RecyclerView.Adapter<AllAdapter.MyViewHolder> {
    private ArrayList<String> horizontalList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout topLL;


        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            topLL = itemView.findViewById(R.id.topLL);
        }
    }


    public AllAdapter(ArrayList<String> horizontalList, Context mContext) {
        this.horizontalList = horizontalList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_all, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.topLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.goToFragment(context, new ResidentialDetailsFragment(), R.id.fragment_container);
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
