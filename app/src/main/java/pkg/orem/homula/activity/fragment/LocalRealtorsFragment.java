package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;

import pkg.orem.homula.activity.adapter.LocalRealtorsAdapter;

import java.util.ArrayList;

public class LocalRealtorsFragment extends BaseFragment {

    private View mView;
    RecyclerView itemsRV;
    ArrayList<String> homeTopMusicData = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_fav, container, false);

        ((MainActivity) getActivity()).setActionBarCustom("", true);
        setHasOptionsMenu(true);








        initControls();
        return mView;
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        itemsRV = mView.findViewById(R.id.itemsRV);
        itemsRV.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(baseActivity);
        itemsRV.setLayoutManager(layoutManager);
        getData();
    }

    private void getData() {
        homeTopMusicData.clear();
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        LocalRealtorsAdapter genresAdapter = new LocalRealtorsAdapter(homeTopMusicData, baseActivity);
        itemsRV.setAdapter(genresAdapter);
    }

}
