package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.utils.Utils;

public class ProfessionalFinderFragment extends BaseFragment {
    private View view;
    LinearLayout topLL;
    CardView professionalCW, estateCW, propertyCW, buyCW, housCW, condoCW, commericalCW, otherCW;
    Button nextBT, professionalBT;
    TextView buyTV;
    ImageView buyIV;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_professional_finder, container, false);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.proffessional_finder), true);
        init(view);
        return view;
    }

    private void init(View view) {
        professionalCW = view.findViewById(R.id.professionalCW);
        professionalBT = view.findViewById(R.id.professionalBT);
        buyCW = view.findViewById(R.id.buyCW);
        estateCW = view.findViewById(R.id.estateCW);


        topLL = view.findViewById(R.id.topLL);
        buyIV = view.findViewById(R.id.buyIV);
        buyTV = view.findViewById(R.id.buyTV);
        propertyCW = view.findViewById(R.id.propertyCW);
        otherCW = view.findViewById(R.id.otherCW);
        commericalCW = view.findViewById(R.id.commericalCW);
        condoCW = view.findViewById(R.id.condoCW);
        housCW = view.findViewById(R.id.housCW);

        baseActivity.setanimationView(topLL);

        professionalBT.setOnClickListener(this);
        buyCW.setOnClickListener(this);
        housCW.setOnClickListener(this);
        condoCW.setOnClickListener(this);
        commericalCW.setOnClickListener(this);
        otherCW.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.professionalBT:
                professionalCW.setVisibility(View.GONE);
                estateCW.setVisibility(View.VISIBLE);
                baseActivity.setanimationView(estateCW);
                break;
            case R.id.buyCW:
                buyTV.setTextColor(getResources().getColor(R.color.white));
                buyIV.setBackgroundResource(R.drawable.buy_selected);
                buyCW.setCardBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                propertyCW.setVisibility(View.VISIBLE);
                baseActivity.setanimationView(propertyCW);
                break;
            case R.id.otherCW:
                Utils.goToFragment(mContext, new ProfessionalFinderTwoFragment(), R.id.fragment_container);
                break;
            case R.id.condoCW:
                Utils.goToFragment(mContext, new ProfessionalFinderTwoFragment(), R.id.fragment_container);
                break;
            case R.id.commericalCW:
                Utils.goToFragment(mContext, new ProfessionalFinderTwoFragment(), R.id.fragment_container);
                break;
            case R.id.housCW:
                Utils.goToFragment(mContext, new ProfessionalFinderTwoFragment(), R.id.fragment_container);
                break;

        }
    }

}
