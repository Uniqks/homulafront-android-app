package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.activity.adapter.AllAdapter;
import pkg.orem.homula.app.Constants;
import pkg.orem.homula.utils.Utils;

import java.util.ArrayList;

public class AllFragment extends BaseFragment {


    private View mView;
    RecyclerView itemsRV, horizontalItemsRV;
    LinearLayout mapLL, searchLL;
    ArrayList<String> homeTopMusicData = new ArrayList<>();
    private boolean isSearch;
    MenuItem map, list;

    private String mGetClass;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fargment_all, container, false);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mGetClass = getArguments().getString(Constants.mClass);
            ((MainActivity) getActivity()).setActionBarCustom(mGetClass, true);
        }



        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        itemsRV = mView.findViewById(R.id.itemsRV);
        horizontalItemsRV = mView.findViewById(R.id.horizontalItemsRV);
        searchLL = mView.findViewById(R.id.searchLL);
        mapLL = mView.findViewById(R.id.mapLL);
        horizontalItemsRV.setHasFixedSize(true);
        horizontalItemsRV.setLayoutManager(new LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false));

        itemsRV.setHasFixedSize(true);
        itemsRV.setLayoutManager(new LinearLayoutManager(baseActivity));
        getData();
    }

    private void getData() {
        homeTopMusicData.clear();
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        AllAdapter genresAdapter = new AllAdapter(homeTopMusicData, baseActivity);
        itemsRV.setAdapter(genresAdapter);
        horizontalItemsRV.setAdapter(genresAdapter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.all_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        map = menu.findItem(R.id.map);
        list = menu.findItem(R.id.list);
        list.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.map:
                mapLL.setVisibility(View.VISIBLE);
                itemsRV.setVisibility(View.GONE);
                list.setVisible(true);
                map.setVisible(false);
                return true;

            case R.id.list:
                mapLL.setVisibility(View.GONE);
                itemsRV.setVisibility(View.VISIBLE);
                list.setVisible(false);
                map.setVisible(true);
                return true;

            case R.id.filter:
                Utils.goToFragment(mContext, new FilterFragment(), R.id.fragment_container);
                return true;

                case R.id.search:
                if (isSearch) {
                    isSearch = false;
                    searchLL.setVisibility(View.GONE);
                } else {
                    searchLL.setVisibility(View.VISIBLE);
                    isSearch = true;
                }
                break;
        }

        return super.onOptionsItemSelected(item); // important line
    }
}
