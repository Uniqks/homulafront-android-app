package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.activity.adapter.AllAdapter;
import pkg.orem.homula.utils.Utils;

import java.util.ArrayList;

public class CamparePropertyFragment extends BaseFragment {

    private View mView;
    RecyclerView itemsRV;
    Button compareBT;
    RelativeLayout topLL;
    ArrayList<String> homeTopMusicData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_compare_property, container, false);
        ((MainActivity) getActivity()).setActionBarCustom("Compare Properties", true);





        initControls();
        return mView;
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        compareBT = mView.findViewById(R.id.compareBT);
        topLL = mView.findViewById(R.id.topLL);
        itemsRV = mView.findViewById(R.id.itemsRV);
        itemsRV.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(baseActivity);
        itemsRV.setLayoutManager(layoutManager);

        compareBT.setOnClickListener(this);
        baseActivity.setanimationView(topLL);
        getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.compareBT:
                Utils.goToFragment(mContext, new ComparsionDetailFragment(), R.id.fragment_container);
                break;
        }
    }

    private void getData() {
        homeTopMusicData.clear();
        homeTopMusicData.add("");
        homeTopMusicData.add("");
        AllAdapter genresAdapter = new AllAdapter(homeTopMusicData, baseActivity);
        itemsRV.setAdapter(genresAdapter);
    }
}
