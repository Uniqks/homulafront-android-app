package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elyeproj.loaderviewlibrary.LoaderTextView;
import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;

public class PrivacyFragment extends BaseFragment {


    private View mView;
    LoaderTextView contentTV;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.privacy_policy), false);





        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {
        contentTV = mView.findViewById(R.id.contentTV);
        contentTV.resetLoader();
        setHandler();
    }

    private void setHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                contentTV.setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
            }
        }, 1000);
    }
}

