package pkg.orem.homula.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pkg.orem.homula.R;
import pkg.orem.homula.activity.viewActivity.MainActivity;
import pkg.orem.homula.utils.Utils;

public class ResidentialDetailsFragment extends BaseFragment {


    private View mView;
    RelativeLayout topLL;
    ImageView closeIV, contactIV;
    LinearLayout contactLL, bottomSheetLayout;
    CardView belowPropertyLL, byTextCW;
    TextView contactTV;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_residential_details, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setActionBarCustom(baseActivity.getString(R.string.details), true);






        initControls();
        return mView;
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        topLL = mView.findViewById(R.id.topLL);
        contactIV = mView.findViewById(R.id.contactIV);
        contactTV = mView.findViewById(R.id.contactTV);
        closeIV = mView.findViewById(R.id.closeIV);
        belowPropertyLL = mView.findViewById(R.id.belowPropertyLL);
        byTextCW = mView.findViewById(R.id.byTextCW);
        contactLL = mView.findViewById(R.id.contactLL);
        bottomSheetLayout = mView.findViewById(R.id.bottomSheetLayout);

        Animation animation1 = AnimationUtils.loadAnimation(baseActivity, R.anim.fade);
        topLL.startAnimation(animation1);

        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactIV.setBackgroundResource(R.drawable.contact_propert_selecty);
                contactTV.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                Animation bottomUps = AnimationUtils.loadAnimation(baseActivity,
                        R.anim.slide_bottom);
                belowPropertyLL.startAnimation(bottomUps);
                belowPropertyLL.setVisibility(View.GONE);
                Animation bottomUp = AnimationUtils.loadAnimation(baseActivity,
                        R.anim.slide_up);
                bottomSheetLayout.startAnimation(bottomUp);
                bottomSheetLayout.setVisibility(View.VISIBLE);
            }
        });

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactIV.setBackgroundResource(R.drawable.contact_propert_selecty);
                contactTV.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                bottomSheetLayout.setVisibility(View.GONE);
                Animation bottomUps = AnimationUtils.loadAnimation(baseActivity,
                        R.anim.slide_bottom);
                bottomSheetLayout.startAnimation(bottomUps);
                bottomSheetLayout.setVisibility(View.GONE);
                Animation bottomUp = AnimationUtils.loadAnimation(baseActivity,
                        R.anim.slide_up);
                belowPropertyLL.startAnimation(bottomUp);
                belowPropertyLL.setVisibility(View.VISIBLE);
            }
        });
        byTextCW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.goToFragment(mContext, new ContactPropertyFragment(), R.id.fragment_container);
            }
        });
        contactIV.setBackgroundResource(R.drawable.contact_property);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

        }

        return super.onOptionsItemSelected(item); // important line
    }
}
